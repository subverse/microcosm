from importlib import reload

from . import mc

mods = [
    mc,
]

for mod in mods:
    reload(mod)
