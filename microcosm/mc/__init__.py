from importlib import reload

from . import api

mods = [
    api,
]

for mod in mods:
    reload(mod)
