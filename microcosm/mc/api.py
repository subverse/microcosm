import os
import json
import logging
import gzip
from pathlib import Path

from pkg_resources import resource_filename as _resource_filename
import requests
import requests.auth
import mediacloud
import tokenmanager
from toolz import pipe, curry, concatv, merge, dissoc, partial
from toolz.curried import (
    map, filter, mapcat,
)

from larcutils.rest import Api, Endpoint
from larcutils.common import (
    vmap, vfilter,
)

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())

resource_filename = partial(_resource_filename, __name__)

class MediacloudAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, request):
        request.prepare_url(request.url, {'key': self.token})
        return request


def get_api():
    return Api(
        'https://api.mediacloud.org/api/v2/',
        MediacloudAuth(tokenmanager.get_tokens().mediacloud),
        requests.Session(),
    )

@curry
def iter_method(method, start_key: str, id_key: str, **kw):
    start = 0
    rows = 100
    params = kw.pop('params', {})
    while True:
        final_params = merge(
            params, {start_key: start, 'rows': rows},
        )
        response = method(params=final_params, **kw)
        if response.status_code == 200:
            data = response.json()
            if not data:
                return
            for datum in data:
                yield datum
            start = max(start + rows, data[-1][id_key])
        else:
            raise ValueError(
                f'bad response code: {response.status_code}\n\n'
                f'{response.content.decode()[:500]}'
            )

media_list_iter = iter_method(
    start_key='last_media_id', id_key='media_id',
)

def get_sources(api: Api):
    return media_list_iter(api('media', 'list').get)

    # start = 0
    # rows = 100
    # while True:
    #     params = {'last_media_id': start, 'rows': rows}
    #     response = api('media', 'list').get(params=params)
    #     if response.status_code == 200:
    #         sources = response.json()
    #         if not sources:
    #             return
    #         for source in sources:
    #             yield source
    #     start += rows

def source_list():
    with open('sources.nljson') as rfp:
        for line in rfp:
            yield json.loads(line)

def get_tags(sources):
    keys = {'description', 'label', 'tag',
            'tag_set', 'tag_sets_id', 'tags_id'}

    all_tags = set()
    for source in sources:
        tags = pipe(
            source['media_source_tags'],
            map(lambda s: tuple((k, v) for k, v in s.items() if k in keys)),
            set,
        )
        for t in tags - all_tags:
            yield dict(t)
        all_tags.update(tags)

@curry
def sources_by_tag(api, tags_id):
    return media_list_iter(
        api('media', 'list').get, params={'tags_id': tags_id},
    )

def get_state_sources():
    return pipe(
        'data/state_sources.json.gz',
        resource_filename,
        gzip.open,
        json.load,
    )
